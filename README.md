
暴走皮皮虾之代码发布系统,是现代的持续集成发布系统,由后台管理系统和agent两部分组成,一个运行着的agent就是一个节点,本系统并不是造轮子,是"鸟枪"到"大炮"的创新,对"前朝遗老"的革命.

[![stable](https://img.shields.io/badge/stable-stable-green.svg)](https://github.com/bzppx/bzppx-codepub/) [![license](https://img.shields.io/github/license/bzppx/bzppx-codepub.svg?style=plastic)]() [![download_count](https://img.shields.io/github/downloads/bzppx/bzppx-codepub/total.svg?style=plastic)](https://github.com/bzppx/bzppx-codepub/releases) [![download](https://img.shields.io/github/release/bzppx/bzppx-codepub.svg?style=plastic)](https://github.com/bzppx/bzppx-codepub/releases)   


# 注意

## 本项目码云地址已迁移到组织 bzppx 下面，地址: https://gitee.com/bzppx/bzppx-codepub


# 反馈

欢迎提交意见和代码 https://github.com/bzppx/bzppx-codepub/issues
官方交流 QQ 群：547481058

## License

MIT

谢谢
---
Create By BZPPX